# DataLad tutorial

Данный репозиторий является приложением к статье, опубликованной в моём канале [iloncka4ds](https://dzen.ru/id/6585a78141d6bb6c240462e1) на https://dzen.ru/ - [Изучаем DataLad: систему управления данными и совместной работы](https://dzen.ru/media/id/6585a78141d6bb6c240462e1/izuchaem-datalad-sistemu-upravleniia-dannymi-i-sovmestnoi-raboty-6585a963a4afb845b1cf4ab6) и содержит ноутбук с примерами использования `DataLad`

### Полезные ресурсы для изучения DataLad

1. [DataLad handbook](https://handbook.datalad.org/en/latest/index.html)
2. [YODA: Best practices for data analyses in a dataset](https://handbook.datalad.org/en/latest/basics/101-127-yoda.html)
3. [Automatically Reproducible Paper Template](https://github.com/datalad-handbook/repro-paper-sketch)
4. [YODA-compliant data analysis projects](https://handbook.datalad.org/en/latest/basics/101-130-yodaproject.html#yoda-project)
5. [DataLad for reproducible machine-learning analyses](https://handbook.datalad.org/en/latest/usecases/ml-analysis.html)
6. [Writing a reproducible paper](https://handbook.datalad.org/en/latest/usecases/reproducible-paper.html)
7. [Research Data Management with DataLad](https://psychoinformatics-de.github.io/rdm-course/)
8. [DataLad Workshop - UKE Hamburg](https://www.youtube.com/watch?v=40ZcGp2vHXk&list=PLEQHbPfpVqU5RSPiyFuPdDlSUEd-XoPV-)
9. [Scaling up: Managing 80TB and 15 million files from the HCP release](https://handbook.datalad.org/en/latest/usecases/HCP_dataset.html)
10. [Neurohackdemy 2022: Data Management for Neuroimaging with DataLad](https://handbook.datalad.org/en/latest/code_from_chapters/neurohackademy.html)
11. [An introduction to DataLad at the Open Science Office Hour](https://handbook.datalad.org/en/latest/code_from_chapters/osoh.html)
12. [A basic automatically and computationally reproducible neuroimaging analysis](https://handbook.datalad.org/en/latest/usecases/reproducible_neuroimaging_analysis_simple.html)
13. [An automatically and computationally reproducible neuroimaging analysis from scratch](https://handbook.datalad.org/en/latest/usecases/reproducible_neuroimaging_analysis.html)
14. [OpenNeuro Quickstart Guide: Accessing OpenNeuro datasets via DataLad](https://handbook.datalad.org/en/latest/usecases/openneuro.html)
15. [DataLad cheat sheet](https://handbook.datalad.org/en/latest/basics/101-136-cheatsheet.html)
16. [GIN - Modern Research Data Management for Neuroscience](https://gin.g-node.org/)
17. [Walk-through: Dataset hosting on GIN](http://docs.datalad.org/projects/gooey/en/latest/gin.html)
